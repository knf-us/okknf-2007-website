-- phpMyAdmin SQL Dump
-- version 2.9.2
-- http://www.phpmyadmin.net
-- 
-- Host: localhost
-- Generation Time: Feb 18, 2007 at 06:55 PM
-- Server version: 5.0.32
-- PHP Version: 5.1.6-pl8-gentoo
-- 
-- Database: `konf2007`
-- 

-- --------------------------------------------------------

-- 
-- Table structure for table `log`
-- 

DROP TABLE IF EXISTS `log`;
CREATE TABLE IF NOT EXISTS `log` (
  `login` varchar(30) collate utf8_polish_ci NOT NULL,
  `when` datetime NOT NULL,
  `table` varchar(255) collate utf8_polish_ci NOT NULL,
  `for` varchar(255) collate utf8_polish_ci NOT NULL,
  `changes` text collate utf8_polish_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- 
-- Dumping data for table `log`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `unis`
-- 

DROP TABLE IF EXISTS `unis`;
CREATE TABLE IF NOT EXISTS `unis` (
  `id` tinyint(4) NOT NULL auto_increment,
  `short` varchar(12) collate utf8_polish_ci NOT NULL,
  `name` varchar(255) collate utf8_polish_ci NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=40 ;

-- 
-- Dumping data for table `unis`
-- 

INSERT INTO `unis` VALUES (1, 'pb', 'Politechnika Białostocka');
INSERT INTO `unis` VALUES (2, 'pc', 'Politechnika Częstochowska');
INSERT INTO `unis` VALUES (3, 'pg', 'Politechnika Gdańska');
INSERT INTO `unis` VALUES (4, 'pk', 'Politechnika Koszalińska');
INSERT INTO `unis` VALUES (5, 'pkr', 'Politechnika Krakowska im. Tadeusza Kościuszki');
INSERT INTO `unis` VALUES (6, 'pl', 'Politechnika Lubelska');
INSERT INTO `unis` VALUES (7, 'plo', 'Politechnika Łódzka');
INSERT INTO `unis` VALUES (8, 'po', 'Politechnika Opolska');
INSERT INTO `unis` VALUES (9, 'pp', 'Politechnika Poznańska');
INSERT INTO `unis` VALUES (10, 'pra', 'Politechnika Radomska im. Kazimierza Pułaskiego');
INSERT INTO `unis` VALUES (11, 'prz', 'Politechnika Rzeszowska im. Ignacego Łukasiewicza');
INSERT INTO `unis` VALUES (12, 'psz', 'Politechnika Szczecińska');
INSERT INTO `unis` VALUES (13, 'psl', 'Politechnika Śląska');
INSERT INTO `unis` VALUES (14, 'psw', 'Politechnika Świętokrzyska');
INSERT INTO `unis` VALUES (15, 'pw', 'Politechnika Warszawska');
INSERT INTO `unis` VALUES (16, 'pwr', 'Politechnika Wrocławska');
INSERT INTO `unis` VALUES (17, 'ug', 'Uniwersytet Gdański');
INSERT INTO `unis` VALUES (18, 'uam', 'Uniwersytet im. Adama Mickiewicza');
INSERT INTO `unis` VALUES (19, 'uj', 'Uniwersytet Jagielloński');
INSERT INTO `unis` VALUES (20, 'uksw', 'Uniwersytet Kardynała Stefana Wyszyńskiego w Warszawie');
INSERT INTO `unis` VALUES (21, 'ukw', 'Uniwersytet Kazimierza Wielkiego');
INSERT INTO `unis` VALUES (22, 'ul', 'Uniwersytet Łódzki');
INSERT INTO `unis` VALUES (23, 'umcs', 'Uniwersytet Marii Curie-Skłodowskiej');
INSERT INTO `unis` VALUES (24, 'umk', 'Uniwersytet Mikołaja Kopernika');
INSERT INTO `unis` VALUES (25, 'uo', 'Uniwersytet Opolski');
INSERT INTO `unis` VALUES (26, 'upr', 'Uniwersytet Przyrodniczy we Wrocławiu');
INSERT INTO `unis` VALUES (27, 'urz', 'Uniwersytet Rzeszowski');
INSERT INTO `unis` VALUES (28, 'usz', 'Uniwersytet Szczeciński');
INSERT INTO `unis` VALUES (29, 'us', 'Uniwersytet Śląski w Katowicach');
INSERT INTO `unis` VALUES (30, 'utp', 'Uniwersytet Technologiczno-Przyrodniczy im. Jana i Jędrzeja Śniadeckich w Bydgoszczy');
INSERT INTO `unis` VALUES (31, 'uwm', 'Uniwersytet Warmińsko-Mazurski w Olsztynie');
INSERT INTO `unis` VALUES (32, 'uw', 'Uniwersytet Warszawski');
INSERT INTO `unis` VALUES (33, 'ub', 'Uniwersytet w Białymstoku');
INSERT INTO `unis` VALUES (34, 'uwr', 'Uniwersytet Wrocławski');
INSERT INTO `unis` VALUES (35, 'uz', 'Uniwersytet Zielonogórski');
INSERT INTO `unis` VALUES (36, 'agh', 'Akademia Górniczo-Hutnicza im. St. Staszica');
INSERT INTO `unis` VALUES (37, 'ajd', 'Akademia im. Jana Długosza w Częstochowie');
INSERT INTO `unis` VALUES (38, 'apken', 'Akademia Pedagogiczna im. Komisji Edukacji Narodowej w Krakowie');
INSERT INTO `unis` VALUES (39, 'as', 'Akademia Świętokrzyska im. Jana Kochanowskiego');

-- --------------------------------------------------------

-- 
-- Table structure for table `users`
-- 

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `login` varchar(30) collate utf8_polish_ci NOT NULL,
  `passwd` varchar(40) collate utf8_polish_ci NOT NULL,
  `desc` varchar(128) collate utf8_polish_ci default NULL,
  `degree` enum('none','inz','mgr','mgr inz','dr','dr hab','doc','prof') collate utf8_polish_ci NOT NULL,
  `name` varchar(50) collate utf8_polish_ci NOT NULL,
  `surname` varchar(75) collate utf8_polish_ci NOT NULL,
  `email` varchar(128) collate utf8_polish_ci NOT NULL,
  `gg` int(11) NOT NULL,
  `jabber` varchar(100) collate utf8_polish_ci NOT NULL,
  `phone` varchar(20) collate utf8_polish_ci NOT NULL,
  `address` varchar(128) collate utf8_polish_ci NOT NULL,
  `city` varchar(75) collate utf8_polish_ci NOT NULL,
  `zip` varchar(7) collate utf8_polish_ci NOT NULL,
  `food` enum('std','veg') collate utf8_polish_ci NOT NULL,
  `university` tinyint(4) NOT NULL,
  `org` varchar(255) collate utf8_polish_ci NOT NULL,
  `comments` text collate utf8_polish_ci NOT NULL,
  `room` enum('nopref','single','double') collate utf8_polish_ci NOT NULL,
  `lecture` text collate utf8_polish_ci NOT NULL,
  `lecture_title` tinytext collate utf8_polish_ci NOT NULL,
  `poster` tinyint(4) NOT NULL,
  `poster_title` tinytext collate utf8_polish_ci NOT NULL,
  `nip` varchar(15) collate utf8_polish_ci NOT NULL,
  `regtime` datetime NOT NULL,
  `active` tinyint(4) NOT NULL,
  `confcode` varchar(100) collate utf8_polish_ci NOT NULL,
  `ip` varchar(16) collate utf8_polish_ci NOT NULL,
  `uid` varchar(32) collate utf8_polish_ci NOT NULL,
  `session` varchar(100) collate utf8_polish_ci NOT NULL,
  `timestamp` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- 
-- Dumping data for table `users`
-- 

