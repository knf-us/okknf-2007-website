<h2>O Cieszynie</h2>

<img src="img/herb_cieszyn.png" alt="herb Cieszyna" style="float: right; padding-left: 10px;" />

<p>Malowniczo położony u podnóża Beskidu Śląskiego, nad rzeką Olzą, Cieszyn jest
historyczną stolicą regionu. Podanie głosi, że w 810 roku trzej bracia: Cieszko,
Bolko i Mieszko, spotkali się przy tutejszym źródle po długiej wędrówce i ciesząc się
tym faktem założyli miasto Cieszyn. Do dzisiaj Studnia Trzech Braci ma upamiętniać to
wydarzenie. Badania naukowe stwierdzają jednak, że historia cieszyńskiego grodu sięga
znacznie dalej, niż głosi podanie.</p>

<p>Obecnie miasto podzielone jest wzdłuż Olzy na części polską i czeską, przy czym
historyczna część miasta pozostała po stronie polskiej. Przez długie lata Cieszyn
był jednym z najważniejszych grodów w tej części Europy, jednak w XX w. utracił swą
dominującą pozycję w regionie na rzecz przemysłowych Bielska i Ostrawy, co utrwalił
jeszcze podział miasta w 1920 roku, który sprowadził Cieszyn do roli miasta granicznego.</p>

<p>Wichry XX-wiecznych wojen oszczędziły ten stary piastowski gród, co przy niewielkim
stopniu uprzemysłowienia i rozbudowy pozwoliło zachować Cieszynowi jego specyficzny
charakter i klimat. Składa się na nie zarówno piękno starego miasta, z niezliczonymi
zabytkami, jak i stosunkowo duża ilość rdzennej ludności, często rozpamiętującej dawną
świetność i niezależność swego miasta i regionu.</p>

<p>Nazywany "małym Wiedniem" Cieszyn jest podstawowym punktem programu każdego szanującego
się turysty przebywającego w okolicy. Mimo niewielkich rozmiarów miasto pozostało
ważnym ośrodkiem kulturalnym, oświatowym i administracyjnym, a część zabytkowa
miasta może ująć naprawdę każdego...</p>

(<a href="http://www.ksiestwocieszynskie.republika.pl/cieszyn.html">żródło</a>)

<h2>Zabytki</h2>

<img src="img/cieszyn_rotunda.jpg" alt="rotunda w Cieszynie" style="float: left; padding-right: 10px;" />

<p>Okolice, jak i sam Cieszyn, posiadają wiele zabytków. Jednym z najbardziej znanych
i charakterystycznych dla Cieszyna jest Rotunda romańska z XI w. w kościele
św. Mikołaja i św. Wacława, przedstawiona na zdjęciu obok.</p>

<p>Wiele ciekawych informacji na temat zabytków w Cieszynie można znaleźć na
<a href="http://www.ksiestwocieszynskie.republika.pl/zabcieszyn.html">stronie
poświęconej księstwu cieszyńskiemu</a>.</p>

<br style="clear: both;" />

<h2>Warto zobaczyć</h2>

<p>Za serwisem <a href="http://www.cieszyn.pl/">cieszyn.pl</a> podajemy listę kilku
obiektów wartych odwiedzenia w Cieszynie:</p>

<ul>
<li><a href="http://www.cieszyn.pl/?p=categoriesShow&amp;iCategory=134">Muzeum Śląska Cieszyńskiego</a>,
jedno z najstarszych muzeów w Europie,</li>
<li><a href="http://www.cieszyn.pl/?p=categoriesShow&amp;iCategory=135">Książnica Cieszyńska</a>,</li>
<li><a href="http://www.cieszyn.pl/?p=categoriesShow&amp;iCategory=136">Góra Zamkowa</a>,</li>
<li><a href="http://www.cieszyn.pl/?p=categoriesShow&amp;iCategory=137">Rynek</a> z końca XV w.,</li>
<li><a href="http://www.cieszyn.pl/?p=categoriesShow&amp;iCategory=138">Studnia Trzech Braci</a>, nawiązująca
do legendy o powstaniu miasta,</li>
<li><a href="http://www.cieszyn.pl/?p=categoriesShow&amp;iCategory=139">Teatr</a>,</li>
<li><a href="http://www.cieszyn.pl/?p=categoriesShow&amp;iCategory=359">Zabytki żydowskie</a>,</li>
<li><a href="http://www.cieszyn.pl/?p=categoriesShow&amp;iCategory=360">Kościoły</a>,</li>
<li><a href="http://www.cieszyn.pl/?p=categoriesShow&amp;iCategory=140">Cieszyńska Madonna</a>, powstała około 1375 roku w praskim warsztacie Piotra Parlera,</li>
<li><a href="http://www.cieszyn.pl/?p=categoriesShow&amp;iCategory=141">Szlak kwitnącej magnolii</a>,</li>
<li><a href="http://www.cieszyn.pl/?p=addressesShow&amp;iAddress=1254&amp;iCategory=142">Muzeum Drukarstwa</a></li>
</ul>

