<h2>Strona główna</h2>
<p><a href="http://knf.us.edu.pl/">Koło Naukowe Fizyków UŚ</a> serdecznie zaprasza do udziału
w VI Ogólnopolskiej Konferencji Kół Naukowych Fizyków <b>"Piknik Naukowy 2007"</b>, która odbędzie
się w Centrum Konferencyjnym Uniwersytetu Śląskiego w <b>Cieszynie</b> w dniach <b>26-29 kwietnia 2007 r</b>.</p>

<h2>Forma konferencji</h2>

<p>"Piknik Naukowy" jest trzydniową imprezą organizowaną przez studentów dla
studentów. Odbywa się corocznie na wiosnę (kwiecień, maj). Jej formuła to
połączenie konferencji naukowej z luźniejszymi formami spędzania czasu.</p>

<p>Podczas konferencji odbywają się m.in. wykłady znakomitych przedstawicieli
polskiej nauki, jak również seminaria wygłaszane przez uczestników konferencji
-- studentów, którzy mają możliwość zaprezentowania swoich osiągnięć oraz
zainteresowań. Tradycyjnie podczas konferencji przewidziany jest wieczorek
integracyjny mający na celu nawiązanie bliższej znajomości oraz współpracy
między pracownikami naukowymi a studentami.</p>


