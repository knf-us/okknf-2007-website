<h2>Program naukowy konferencji</h2>

<table width="100%" border="0" cellspacing="0" cellpadding="0" class="schedule">
<tr><th colspan="3">Czwartek, 26. kwietnia 2007 r.</th></tr>
<tr><td>08:00-13:30</td><td>Rejestracja uczestników</td><td><abbr title="Akademik">DSN</abbr></td></tr>
<tr class="food"><td>13:30-15:00</td><td>Obiad</td><td>Stołówka</td></tr>

<tr><td>15:00-16:30</td><td colspan="2">Czas wolny</td></tr>
<tr class="lect"><td>16:30-18:00</td><td>Wykłady inauguracyjne</td><td><abbr title="Centrum Konferencyjne">CK</abbr></td></tr>
<tr><td>16:30</td><td colspan="2"><span class="author">prof. dr hab. Marek Szopa</span><br />Fullereny i nanorurki węglowe - od syntezy do zastosowań</td></tr>
<tr><td>17:15</td><td colspan="2"><span class="author">prof. dr hab. Henryk Czyż</span><br />LHC - rachunek sumienia przed wielkim eksperymentem</td></tr>
<tr class="food"><td>18:00-20:00</td><td>Kolacja, uroczyste ropoczęcie konferencji</td><td>Stołówka</td></tr>

<tr><td colspan="3">&nbsp;</td></tr>

<tr><th colspan="3">Piątek, 27. kwietnia 2007 r.</th></tr>
<tr class="food"><td>08:30-10:00</td><td>Śniadanie</td><td>Stołówka</td></tr>
<tr class="lect"><td>10:30-14:00</td><td>I sesja wykładowa: Metody matematyczne i komputerowe fizyki<br />Chairman: Andrzej Ptok</td><td><abbr title="Centrum Konferencyjne">CK</abbr></td></tr>

<tr><td>10:30</td><td colspan="2"><span class="author">Tomasz Kozioł</span><br />Symulacja przy użyciu algorytmu Metropolisa</td></tr>
<tr><td>10:50</td><td colspan="2"><span class="author">Katarzyna Bartuś</span><br />Symulacja dynamiki atomów Ar w nanorurce</td></tr>
<tr><td>11:20</td><td colspan="2"><span class="author">Michał Januszewski</span><br />Metoda cząstek rozmytych w hydrodynamice (SPH)</td></tr>
<tr class="food"><td>11:50</td><td colspan="2">Przerwa na kawę</td></tr>
<tr><td>12:05</td><td colspan="2"><span class="author">Magdalena Zych</span><br />Butelka Kleina</td></tr>

<tr><td>12:25</td><td colspan="2"><span class="author">Adam Siwecki</span><br />Transformacja Darboux jako narzędzie</td></tr>
<tr><td>12:45</td><td colspan="2"><span class="author">Dorota Kucharczyk</span><br />Sposoby opisu informacji ciągłych</td></tr>
<tr><td>13:05</td><td colspan="2"><span class="author">Mateusz Kuszner</span><br />Rozwiązanie układu AKNS za pomocą transformacji Backlunda</td></tr>
<tr><td>13:25</td><td colspan="2"><span class="author">Agnieszka Leyko</span><br />Formalizm Weyla-Wignera-Moyala</td></tr>
<tr class="food"><td>14:00-15:30</td><td>Obiad</td><td>Stołówka</td></tr>

<tr class="lect"><td>15:30-19:00</td><td>II sesja wykładowa: Fizyka teoretyczna I<br />Chairman: Katarzyna Bartuś</td><td><abbr title="Centrum Konferencyjne">CK</abbr></td></tr>
<tr><td>15:30</td><td colspan="2"><span class="author">dr Jerzy Król</span><br />Podstawy matematyki, egzotyczne R^4 i modele czasoprzestrzeni</td></tr>
<tr><td>16:20</td><td colspan="2"><span class="author">Jacek Puchta</span><br />Kwantowa grawitacja pętlowa</td></tr>
<tr><td>16:50</td><td colspan="2"><span class="author">Andrzej Ptok</span><br />Niekonwencjonalne nadprzewodnictwo</td></tr>

<tr class="food"><td>17:10</td><td colspan="2">Przerwa na kawę</td></tr>
<tr><td>17:25</td><td colspan="2"><span class="author">Marcin Markiewicz</span><br />Równanie Abrahama-Lorentza a łamanie zasady przyczynowości. O problemach klasycznej teorii cząstek naładowanych.</td></tr>
<tr><td>18:05</td><td colspan="2"><span class="author">Magdalena Gulewicz</span><br />Grafen - relatywistyka w śladzie ołówka</td></tr>
<tr><td>18:35</td><td colspan="2"><span class="author">Bartłomiej Szczygieł</span><br />Nowy sposób zerwania nitki</td></tr>
<tr class="food"><td>19:00-20:00</td><td>Kolacja</td><td>Stołówka</td></tr>

<tr><td>20:00-...</td><td>Impreza integracyjna</td><td>Klub studencki</td></tr>

<tr><td colspan="3">&nbsp;</td></tr>

<tr><th colspan="3">Sobota, 28. kwietnia 2007 r.</th></tr>
<tr class="food"><td>09:00-10:30</td><td>Śniadanie</td><td>Stołówka</td></tr>
<tr class="lect"><td>10:30-14:00</td><td>III sesja wykładowa: Fizyka doświadczalna<br />Chairman: Sebastian Szwarc</td><td><abbr title="Centrum Konferencyjne">CK</abbr></td></tr>

<tr><td>10:30</td><td colspan="2"><span class="author">Mirosław Salamończyk</span><br />Ciekłe kryształy - droga do ich lepszego poznania</td></tr>
<tr><td>10:50</td><td colspan="2"><span class="author">Aleksandra Mielewczyk</span><br />Między fizyką a chemią - polimery przewodzące</td></tr>
<tr><td>11:10</td><td colspan="2"><span class="author">Alicja Szydłowska</span><br />Ogniwa fotowoltaiczne na bazie materiałów organicznych</td></tr>
<tr><td>11:30</td><td colspan="2"><span class="author">Michał Rawski</span><br />Ogniwa fotowoltaiczne na porowatym Si</td></tr>
<tr class="food"><td>11:50</td><td colspan="2">Przerwa na kawę</td></tr>

<tr><td>12:05</td><td colspan="2"><span class="author">Joanna Oracz</span><br />Cieczowy modulator akustooptyczny</td></tr>
<tr><td>12:25</td><td colspan="2"><span class="author">Anna Majcher</span><br />Skupianie wiązki promieniowania synchrotronowego za pomocą sagitalnie zgiętych kryształów Lauego</td></tr>
<tr><td>12:45</td><td colspan="2"><span class="author">Przemysław Adamkiewicz</span><br />Teleskop ANTARES - detektor wysokoenergetycznych neutrin</td></tr>
<tr><td>12:55</td><td colspan="2"><span class="author">Maciej Rogala</span><br />Kropki kwantowe</td></tr>
<tr><td>13:20</td><td colspan="2"><span class="author">Paweł Barbarski</span><br />Napęd jonowy - napędem przyszłości?</td></tr>

<tr class="food"><td>14:00-15:30</td><td>Obiad</td><td>Stołówka</td></tr>
<tr class="lect"><td>15:00-19:00</td><td>IV sesja wykładowa: Fizyka teoretyczna II, Astrofizyka<br />Chairman: Agnieszka Grzanka</td><td><abbr title="Centrum Konferencyjne">CK</abbr></td></tr>
<tr><td>15:30</td><td colspan="2"><span class="author">prof. dr hab. Elżbieta Zipper</span><br />Małe jest wielkie czyli o nanoukładach.</td></tr>
<tr><td>16:15</td><td colspan="2"><span class="author">Michał Ochman</span><br />Efekt MSW</td></tr>

<tr><td>16:40</td><td colspan="2"><span class="author">Tomasz Tylec</span><br />Doświadczenie Francka-Hertza - szczęśliwy zbieg okoliczności</td></tr>
<tr class="food"><td>17:00</td><td colspan="2">Przerwa na kawę</td></tr>
<tr><td>17:15</td><td colspan="2"><span class="author">Łukasz Kochańczyk</span><br />Eta Carinae - nietypowy sąsiad w naszej galaktyce</td></tr>
<tr><td>17:35</td><td colspan="2"><span class="author">Wojciech Makowiecki</span><br />Metody automatycznego rozpoznawania fragmentów nieba</td></tr>
<tr><td>17:55</td><td colspan="2"><span class="author">Sebastian Szwarc</span><br />IAPS/ICPS</td></tr>

<tr><td>18:15</td><td colspan="2"><span class="author">Weronika Pelc</span><br />Sekcja teoretyczna Koła Naukowego Studentów Fizyki Politechniki Gdańskiej - SOLITON</td></tr>
<tr class="lect"><td>18:35-19:00</td><td>Sesja posterowa</td><td><abbr title="Centrum Konferencyjne">CK</abbr></td></tr>
<tr class="food"><td>19:00-20:00</td><td>Kolacja</td><td>Stołówka</td></tr>
<tr><td>20:00-...</td><td colspan="2">Czas wolny, proponowana wycieczka do Czeskiego Cieszyna</td></tr>

<tr><td colspan="3">&nbsp;</td></tr>

<tr><th colspan="3">Niedziela, 29. kwietnia 2007 r.</th></tr>
<tr class="food"><td>09:00-10:30</td><td>Śniadanie</td><td>Stołówka</td></tr>
<tr class="lect"><td>10:30-12:30</td><td>V sesja wykładowa: Fizyka medyczna<br />Chairman: Bartłomiej Szczygieł</td><td><abbr title="Centrum Konferencyjne">CK</abbr></td></tr>
<tr><td>10:30</td><td colspan="2"><span class="author">Monika Zubik</span><br />Nanocząstki w służbie medycynie</td></tr>
<tr><td>10:50</td><td colspan="2"><span class="author">Agata Kierzek</span><br />Druty dla stomatologii</td></tr>

<tr><td>11:10</td><td colspan="2"><span class="author">Małgorzata Nakonieczna</span><br />Mechanizm przewodzenia impulsów nerwowych</td></tr>
<tr><td>11:30</td><td colspan="2"><span class="author">Anna Pasztuszczak</span><br />Fotoablacja laserowa w chirurgii refrakcyjnej rogówki</td></tr>
<tr><td>12:50</td><td colspan="2"><span class="author">Marta Mocna</span><br />Radioterapia stereotaktyczna</td></tr>
<tr class="food"><td>12:30-14:30</td><td>Obiad, uroczyste zakończenie konferencji</td><td>Stołówka</td></tr>
</table>

<h2>Plakaty prezentowane podczas sesji posterowej</h2>

<ul>
<li><b>Druty dla stomatologii</b>, Sławomir Domin, Mariusz Janus, Agata Kierzek, Beata Młocek, Magdalena Weber, Tomasz Wojciechowski</li>
<li><b>Dzikie fale</b>, Tomasz Kozioł</li>
<li><b>Koło Naukowe Astronomów (część pierwsza: plakat reklamowy)</b>, Stanisław Janikowski</li>
<li><b>Koło Naukowe Astronomów (część druga: plakat organizacyjny)</b>, Stanisław Janikowski</li>

<li><b>Ogólnopolskie Seminarium Studentów Astronomii 2004</b>, Stanisław Janikowski</li>
<li><b>Otrzymywanie niskich temperatur</b>, Paweł Kowalczuk</li>
<li><b>Radiometeory</b>, Stanisław Janikowski, Marek Górski</li>
<li><b>Rozciągłe obiekty w kosmosie. Mgławice i ich rodzaje.</b>, Maciej Derejski</li>
<li><b>SiO_2 i co dalej?</b>, Natalia Woźnica</li>
<li><b>Sterowanie wektorem ciągu</b>, Katarzyna Bartuś</li>

</ul>


