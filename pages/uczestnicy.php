<h2>Pracownicy naukowi</h2>

<?php

$trans = array("none" => "", "mgr" => "mgr", "inz" => "inż.", "mgr inz" => "mgr inż.",
			   "dr" => "dr", "dr hab" => "dr hab.", "doc" => "doc.", "prof" => "prof. dr hab.");

$q = $dbc->query("SELECT us.degree, us.name, us.surname, ".
				"un.name AS uni, us.desc AS descr FROM ".
				TBL_USER." us LEFT JOIN unis un ON un.id = us.university ".
				"WHERE degree NOT IN ('none','mgr','inz','mgr inz') && active=1 ".
				"ORDER BY surname, name ASC");

echo '<table width="100%" border="0" cellspacing="0" cellpadding="0" class="participants">';
$cnt = 1;

while ($t = $q->fetch(PDO::FETCH_ASSOC)) {
	echo '<tr class="row'.($cnt % 2).'"><td>'.$cnt.'</td><td>'.$trans[$t["degree"]]." ".$t["name"]." ".$t["surname"].'</td><td>';
	echo $t["descr"].'</td></tr>';
	$cnt++;
}

if ($cnt == 1) {
	echo '<tr><td class="row0">Na konferencję nie zarejestrowali się jeszcze żadni pracownicy naukowi.</td></tr>';
}

echo '</table>';

$q->closeCursor();
unset($q);

?>

<h2>Studenci</h2>

<?php

if ($_REQUEST["order"] == "desc") {
	$order = "DESC";
	$iorder = "asc";
} else {
	$order = "ASC";
	$iorder = "desc";
}

if ($_REQUEST["sort"] == "uni") {
	$sort = "un.name";
} else {
	$sort = "surname, name";
}

$q = $dbc->query("SELECT us.degree, us.name, us.surname, ".
				"un.name AS uni, us.desc AS descr FROM ".
				TBL_USER." us LEFT JOIN unis un ON un.id = us.university ".
				"WHERE degree IN ('none','mgr','inz','mgr inz') && active=1 ".
				"ORDER BY $sort $order");

echo <<<HTML
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="participants">
<tr><th>lp</th>
<th><a href="uczestnicy?sort=name&amp;order=$iorder">uczestnik</a></th>
<th><a href="uczestnicy?sort=uni&amp;order=$iorder">uczelnia</th></tr>
HTML;

$cnt = 1;

while ($t = $q->fetch(PDO::FETCH_ASSOC)) {
	echo '<tr class="row'.($cnt % 2).'"><td>'.$cnt.'</td><td>'.$trans[$t["degree"]]." ".$t["name"]." ".$t["surname"].'</td><td>';
	echo $t["uni"].'</td></tr>';
	$cnt++;
}

if ($cnt == 1) {
	echo '<tr><td class="row0">Na konferencję nie zarejestrowali się jeszcze żadni studenci.</td></tr>';
}

echo '</table>';

?>
