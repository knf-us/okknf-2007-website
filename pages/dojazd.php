<h2>Gdzie jest Cieszyn?</h2>

<p style="text-align:center;">
<img src="img/mapka_pl.png" alt="Mapa Polski" /><br />
(na podstawie <a href="http://maps.google.com">Google Maps</a>)
</p>

<h2>Jak dostać się do Cieszyna?</h2>

<p>Do Cieszyna najłatwiej dostać się pociągiem. Istnieje kilka dogodnych połączeń:</p>
<ul>
<li>Katowice - Cieszyn (przez Zebrzydowice)</li>
<li>Kraków - Cieszyn (przez Czechowice-Dziedzice)</li>
<li>Warszawa - Cieszyn (przez Czechowice-Dziedzice)</li>
<li>Poznań/Wrocław - Cieszyn (przez Katowice)</li>
<li>Zielona Góra - Cieszyn (przez Katowice)</li>
</ul>

<p>Więcej informacji na stronie <a href="http://rozklad.pkp.pl/bin/query.exe/pn?">PKP</a>.</p>

<h2>Plan miasta</h2>

<img src="img/mapka_cieszyn.png" alt="Plan Cieszyna" width="580" />
<p>Na planie zaznaczono miejsca zakwaterowania uczestników konferencji oraz dworzec PKP.
Plan opracowany na podstawie internetowego planu Cieszyna z
<a href="http://www.cieszyn.pl/">http://www.cieszyn.pl/</a>.</p>

<p>Możesz również zobaczyć <a href="http://www.cieszyn.pl/_mapa/">interaktywny plan Cieszyna</a>.</p>

<p>Droga z dworca PKP do akademików zajmuje (pieszo) 20-30 minut.</p>

