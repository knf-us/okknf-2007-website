<h2>Informacje ogólne</h2>

<ul>
<li>Jedno koło naukowe może na konferencji reprezentować maksymalnie
7 osób, z czego przynajmniej jedna powinna wygłosić wykład.</li>
<li>Zachęcamy również wszystkich do przygotowywania plakatów na sesję posterową.</li>
<li><b>Ostateczny termin rejestracji i uiszczenia całości opłaty konferencyjnej mija 16. kwietnia 2007!</b></li>
<li>W przypadku chęci wygłoszenia wykładu należy do <b>16. kwietnia 2007</b> podać jego tytuł oraz abstrakt.</li>
<li>Opłata konferencyjna wynosi <b>150 PLN</b> (dla studentów; zakwaterowanie w domu studenckim) 
i <b>270 PLN</b> (dla pracowników naukowych; zakwaterowanie w Hotelu Academicus)</li>
<li>Prosimy o wpłaty <b>całości opłaty konferencyjnej</b> 
na konto: ING BANK ŚLĄSKI, 74 1050 1214 1000 0007 0000 7909, 
z dopiskiem: "Piknik Naukowy 2007"</li>
</ul>

<h2>Rejestracja</h2>

<p>Rejestracji na konferencję można dokonać <a href="reg">na tej stronie</a>.
Przy rejestracji należy wypełnić wszystkie wymagane pola formularza rejestracyjnego.
Po zakończeniu procesu rejestracji otrzymasz konto, które pozwoli Ci na określenie
dodatkowych preferencji oraz zmianę niektórych ustawień w dowolnym momencie od
chwili rejestracji do deadline 16. kwietnia.</p>

<p>W przypadku chęci wygłoszenia wykładu lub zaprezentowania posteru naukowego, nie jest
konieczne podawanie jego tytułu i abstraktu w momencie rejestracji. Dane te mogą być
uzupełnione w dowolnym momencie przed 16. kwietnia.</p>

<h2>Wykłady</h2>

<p>Warunkiem koniecznym wygłoszenia wykładu jest podanie jego abstraktu w formularzu
rejestracyjnym. Abstrakty powinny być napisane w języku polskim. W abstraktach
można używać wyrażeń LaTeX-owych, np. $\int_{-\infty}^{\infty} e^{-x^2} dx = \sqrt{\pi}$
będzie widoczne jako: <img src="img/latex_sample.gif" style="vertical-align:middle;" alt="the gaussian integral" />.</p>

<p>Prezentacje nie powinny być dłuższe niż 20 min, z czego 5 min powinno być poświęcone
na pytania od publiczności. Na miejscu będzie dostępny projektor oraz komputer z oprogramowaniem
Acrobat Reader 7.0, MS PowerPoint 2000 oraz OpenOffice 2.0.</p>


