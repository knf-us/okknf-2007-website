<h2>Organizatorzy</h2>

<img src="img/knf-logo-white_125px.png" alt="logo KNF UŚ" style="width: 125px; float: right; padding-left: 20px;" />
<p>Organizatorami Pikniku Naukowego '2007 są
<a href="http://knf.us.edu.pl/">Koło Naukowe Fizyków Uniwersytetu Śląskiego</a> oraz
Sekcja Młodych Polskiego Towarzystwa Fizycznego.</p>

<p>Komitet Organizacyjny tworzą następujący członkowie KNF UŚ:</p>
<ul>
	<li>Katarzyna Bartuś -- v-ce prezes ds. naukowych KNF UŚ (2006/2007),</li>
	<li>Agnieszka Grzanka -- v-ce prezes ds. finansowych KNF UŚ (2005/2006),</li>
	<li>Michał Januszewski -- prezes KNF UŚ (2006/2007),</li>
</ul>

<h2>Dane kontaktowe</h2>

<p>Wszelkie pytania, zgłoszenia problemów technicznych itp. można kierować
na adres <a href="mailto:konf@knf.us.edu.pl">konf@knf.us.edu.pl</a>.</p>

<p>W razie potrzeby można się z nami również kontaktować telefonicznie dzwoniąc
na numer Koła Naukowego Fizyków UŚ: <b>032 359 16 39</b> lub, w przypadku
pilnych spraw, bezpośrednio z Katarzyną Bartuś pod numerem <b>+48 600 805 265</b>.
</p>


