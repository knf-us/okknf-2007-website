<h2>Zakwaterowanie</h2>

<p>Piknik Naukowy '2007 odbywa się w Centrum Konferencyjnym Uniwersytetu Śląkiego
w Cieszynie. Zakwaterowanie na czas konferencji wiąże się z koniecznością uiszczenia
opłaty konferencyjnej w wysokości:</p>
<ul>
	<li>150 PLN (studenci)</li>
	<li>270 PLN (pracownicy naukowi)</li>
</ul>

<p>W opłatę konferencyjną wliczony jest koszt noclegu w akademiku, wyżywienia (3 posiłki dziennie,
wg programu konferencji) oraz materiałów konferencyjnych. W akademiku na każde dwa pokoje
dwuosobowe przypada łazienka oraz aneks kuchenny. W pobliżu akademików znajduje się dobrze
zaopatrzony sklep.</p>

<h2>Dane kontaktowe</h2>

<img src="img/centrum.jpg" alt="Centrum Konferencyjne UŚ" style="float: left; width: 200px; padding-right: 10px; padding-bottom: 20px;" />
<p>
<b>Wykłady, dyskusje:<br />
Centrum Konferencyjne w Cieszynie</b><br />
43 - 400 Cieszyn, ul. Bielska 62<br />
tel. 033 - 854 61 52<br />
fax. 033 - 854 63 21<br />
</p>

<br style="clear: both;" />

<img src="img/academicus.jpg" alt="zajazd Academicus" style="float: left; width: 200px; padding-right: 10px;" />
<p>
<b>Zakwaterowanie (kadra):<br />
Zajazd Academicus</b><br />
43 - 400 Cieszyn, ul. Paderewskiego 6<br />
tel. 033 - 8546464<br />
fax: 033 - 8546466<br />
</p>

<br style="clear: both;" />

<p>
<b>Zakwaterowanie (studenci):<br />
DSN -- akademik</b><br />
43-400 Cieszyn, ul. Niemcewicza 8<br />
tel. 0338546130 - kierowniczka osiedla<br />
tel./fax. 0338546127 - administracja<br />
tel. 0338546120 - recepcja
</p>

