<?php

$f = new pdataForm($html, $dbc, "register");

if ($_REQUEST["cmd"] == "" && !$ssn->loggedIn()) {

	echo '<h2>Jeśli masz już konto...</h2>';

	$lf = new loginForm($html, $user);
	$lf->printForm();

/*
	echo <<<HTML
<h2>Jeśli chcesz się zarejestrować...</h2>

<p>Przed wypełnieniem formularza rejestracyjnego prosimy o zapoznanie
się z <a href="info">ważnymi informacjami</a> na temat konferencji.</p>
HTML;

	$f->printForm();
*/
	return;
}

echo "<h2>Baza danych Pikniku Naukowego '2007</h2>";

if ($_REQUEST["cmd"] == "register") {
//	cmd_register($f);
	;
} else if ($_REQUEST["cmd"] == "finish") {
//	cmd_finish();
	;
} else if ($_REQUEST["cmd"] == "login") {
	cmd_login();
} else if ($_REQUEST["cmd"] == "logout") {
	cmd_logout();
} else if ($ssn->loggedIn()) {
	echo '<p><a href="'.ROOT_URI.'/reg&cmd=logout">Kliknij tutaj, aby się wylogować.</a></p>';
	cmd_edit();
}

function cmd_edit()
{
	global $html, $dbc, $ssn, $user;

	$login = $ssn->getLogin();
	$f = new pdataForm($html, $dbc, "pdataedit", $login);

	$q = $dbc->prepare("SELECT * FROM ".TBL_USER." WHERE login = ?");
	$q->execute(array($login));
	$r = $q->fetch(PDO::FETCH_ASSOC);
	$q->closeCursor();
	
	if ($_REQUEST["subm"] == "") {
		if ($r["lecture"]) {
			echo '<div class="box infobox abstract"><h1>Podgląd abstraktu</h1><p>'.latex_content($r["lecture"]).'</p></div>';
		}
		
		$f->setFields($r);
		$f->printForm();
		return;
	}

	/* Make sure all input is valid. */
	if ($f->validate($_REQUEST)) {
		$f->printForm();
		return;
	}

	$upd = $f->diffDbFieldsWith($r);
	if ($upd["passwd"] == "") {
		unset($upd["passwd"]);
	} else {
		$upd["passwd"] = sha1($upd["passwd"]);
	}

	/* Nothing to do -- go back to the main page. */
	if (count(array_keys($upd)) == 0) {
		redir();
		return;
	}

	$query = "UPDATE ".TBL_USER." SET ";
	$first = true;
	foreach (array_keys($upd) as $k) {
		if (!$first) {
			$query .= ",";
		}

		$query .= "$k = ?";
		$first = false;
	}

	$query .= " WHERE login = ?";
	$q = $dbc->prepare($query);
	$q->execute(array_merge(array_values($upd), array($login)));

	if ($q->rowCount() == 1) {
		$html->infoBox("<p>Dane zostały uaktualnione.</p>");
		log_change(TBL_USER, $login, $upd);

		$subject = '[konf2007] Zmiana danych';
		$headers = 'From: '.ADMIN_MAIL."\r\n".
			'Reply-To: '.ADMIN_MAIL."\r\n".
			'X-Mailer: PHP';

		$text = $f->textGetFields(array_keys($upd));
		$text .= "changed-by: ".$ssn->getLogin()."\n";

		mail(INFO_MAIL, $subject, $text, $headers);
	} else {
		$html->errorBox("Uaktualnienie danych nie powiodło się. Skontakuj się z administratorem.");
	}
	$q->closeCursor();
}

function cmd_logout()
{
	global $html, $ssn;

	if (!$ssn->logout()) {
		$html->errorBox("<p>Wylogowanie nie powiodło się.</p>");
		return;
	}

	$html->infoBox("<p>Wylogowanie zakończone sukcesem</p>");
	return;
}

function cmd_login()
{
	global $html, $user, $ssn;

	$lf = new loginForm($html, $user);
	if (!$lf->validate($_REQUEST)) {
		$lf->printForm();
		return;
	}

	if ($ssn->loggedIn())
		return;

	if (!$ssn->login($_REQUEST["login"])) {
		$html->errorBox("<p>Logowanie nie powiodło się.</p>");
		$html->infoLink(ROOT_URI.'/index.php', "Przejście do strony logowania");
		return;
	}

	$html->infoBox("<p>Logowanie zakończone pomyślnie.</p>");
	$html->infoLink(ROOT_URI.'/index.php', "Przejście do strony głównej");

	/* Redirect the user to his homepage in the system.
	 * This will work since output buffering is enabled by default. */
	header("Refresh: 0; URL=".ROOT_URI."/reg");
	return;
}

function cmd_finish()
{
	global $user, $html;

	$cnt = $user->activate($_REQUEST["code"]);
	
	if ($cnt > 0) {
		$html->infoBox("<p>Konto zostało aktywowane. Możesz się teraz zalogować.</p>");
		$lf = new loginForm($html, $user);
		$lf->printForm();
	} else {
		$html->errorBox("<p>Aktywacja nie powiodła się. Skontaktuj się z administratorem.</p>");
	}
	return;
}

function cmd_register($f)
{
	global $dbc, $html;

	if ($f->validate($_REQUEST)) {
		$f->printForm();
		return;
	}

	$flds = $f->dbFields();
	$vals = array();

	foreach ($flds as $t) {
		/* We only store a hashed version of the password in the database. */
		if ($t == "passwd") {
			array_push($vals, sha1($f->getField($t)));
		} else {
			array_push($vals, $f->getField($t));
		}
	}	

	array_push($flds, 'regtime');
	array_push($flds, 'active');
	array_push($flds, 'confcode');

	$query = "INSERT INTO ".TBL_USER."(".implode(",", $flds).") VALUES(?";
	for ($i = 1; $i < count($flds) - 3; $i++) {
		$query .= ",?";
	}

	$confcode = md5(rand()."abc123");
	$query .=", NOW(), 0, '$confcode')";
	$q = $dbc->prepare($query);
	$q->execute($vals);

	if ($q->rowCount() == 1) {
		$subject = '[konf2007] Rejestracja';
		$headers = 'From: '.ADMIN_MAIL."\r\n".
					'Reply-To: '.ADMIN_MAIL."\r\n".
					'X-Mailer: PHP';

		mail(INFO_MAIL, $subject, $f->textGetFields(), $headers);

		$addr = $f->getField("email");
		$login = $f->getField("login");

		$subject = "Piknik Naukowy '2007 -- Rejestracja";
		$headers = "From: konf@knf.us.edu.pl\r\n".
					"Reply-To: konf@knf.us.edu.pl\r\n".
					"X-Mailer: PHP";
		$mesg = "Witaj!\n\nAby dokonczyc rejestracje, otworz ponizsza strone:\n".
			ROOT_URI."/reg&cmd=finish&code=${confcode}\n\n".
			"Dziekujemy za rejestracje na konferencje :)\n";

		mail($addr, $subject, $mesg, $headers);

		$html->infoBox("<p>Dziękujemy za rejestrację!</p>".
			"<p>Na podany przy rejestracji adres e-mail została właśnie wysłana wiadomość ".
			"z kodem aktywującym. Po aktywacji konta będzie możliwe zalogowanie się do systemu ".
			"i zmiana niektórych ustawień.</p>");
	} else {
		$html->errorBox("<p>Rejestracja nie powiodła się. Skontakuj się z administratorem.</p>");
	}
	$q->closeCursor();

	return;
}
?>







