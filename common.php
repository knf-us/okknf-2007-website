<?php
	ob_start();

	include_once("config.php");
	include("layout.php");
	include("html.php");
	include("user.php");
	include("session.php");
	include("form.php");
	include_once('latexrender/latex.php');

	$dbc = new PDO('mysql:host='.DB_HOST.';dbname='.DB_NAME, DB_USER, DB_PASS);
	$dbc->query("set charset utf8");
	$html = new HTML;
	$user = new User($html, $dbc);
	$ssn = new Session($html, $user);

	srand();

	mb_language('Neutral');
	mb_internal_encoding("UTF-8");
	mb_http_input("UTF-8");
	mb_http_output("UTF-8");

	get_lang();

function broken_browser()
{
	if (preg_match("/MSIE/", $_SERVER["HTTP_USER_AGENT"])) {
		return true;
	} else {
		return false;
	}
}

function get_lang()
{
	global $lang;
    $lang = 'pl';
	$accepted = array("pl", "en");
	if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
		$langs = explode(';',$_SERVER['HTTP_ACCEPT_LANGUAGE'],2);
		$langs = explode(',',$langs[0]);

		foreach($langs as $t) {
			if (in_array($t, $accepted)) {
				$lang = $t;
				break;
			}
		}
	}

	if (isset($_REQUEST['lang']) && in_array($_REQUEST['lang'], $accepted)) {
		$lang = $_REQUEST['lang'];
	}
}

function get_req_page()
{
	global $menu;

	$page = $_REQUEST["p"];
	$page = preg_replace('/[^a-zA-Z\/0-9\_\-]+/i', '', $page);

	if ($page != "") {
		return "$page";
	} else {
		return "glowna";
	}
}

function log_change($table, $for, $changes)
{
	global $ssn, $dbc;

	$q = $dbc->prepare("INSERT INTO `log` ( `login` , `when` , `table` , `for` , `changes` ) VALUES(?, NOW(), ?, ?, ?)");
	$q->execute(array($ssn->getLogin(), $table, $for, serialize($changes)));
	$q->closeCursor();
}

function redir($url = "")
{
	if ($url == "") {
		$url = ROOT_URI."/";
	}

	/* Redirect the user to his homepage in the system.
	 * This will work since output buffering is enabled by default. */
	header("Refresh: 0; URL=$url");
}


?>
