<?php

class HTML
{
	function stdFooter()
	{
		echo '<hr width="100%" />Kontakt z administratorem: '.ADMIN_NAME.', '.ADMIN_EMAIL_DISPLAY;
		echo '<br \><a href="http://spock.one.pl/gitweb/?p=knf;a=summary">Kod źródłowy systemu</a> (GPL2)';
		echo "</body></html>";
	}

	function errorMsg($msg)
	{
		echo '<p class="error">'.$msg.'</p>'."\n";
	}

	function infoMsg($msg)
	{
		echo '<p class="info">'.$msg.'</p>'."\n";
	}

	function infoLink($to, $desc)
	{
		echo '<p style="text-align: center;"><a href="'.$to.'">'.$desc.'</a></p>'."\n";
	}

	function infoBox($msg, $title = "Informacja")
	{
		echo '<div class="box infobox"><h1>'.$title.'</h1>'.$msg.'</div>';
	}

	function errorBox($msg)
	{
		echo '<div class="box errorbox"><h1>Błąd</h1>'.$msg.'</div>';
	}
}

?>
