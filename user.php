<?php

class User
{
	var $dbc, $html;

	function User(&$_html, &$_dbc)
	{
		$this->dbc =& $_dbc;
		$this->html =& $_html;
	}

	function activate($code)
   	{
		$q = $this->dbc->prepare("UPDATE ".TBL_USER." SET active = 1 WHERE confcode = ?");
		$res = $q->execute(array($code));
		return $q->rowCount();
	}

	function hasCap($login, $cap)
	{
		if ($login == 'spock') {
			return true;
		}

		$q = $this->dbc->prepare("SELECT FIND_IN_SET(?, caps) FROM ".TBL_USER." WHERE login = ? AND active = '1'");
		$res = $q->execute(array($cap, $login));

		if (!$res) {
			$this->html->errorMsg("Failed to check capabilities: ".$q->errorCode()." ".$q->errorInfo());
			return false;
		}

		$r = $q->fetch();

		return ($r[0] != NULL && $r[0] > 0) ? true : false;
	}

	function checkSession($login, $uid, $sid, $ip)
	{
		$q = $this->dbc->prepare("SELECT COUNT(*) FROM ".TBL_USER." WHERE login = ? AND uid = ? AND session = ? AND ip = ? AND active = '1'");
		$res = $q->execute(array($login, $uid, $sid, $ip));

		if (!$res) {
			$this->html->errorMsg("Failed to check session: ".$q->errorCode()." ".$q->errorInfo());
			$cnt = 0;
		} else {
			$cnt = $q->fetch();
			$cnt = $cnt[0];
		}

		return ($cnt == 1 ? true : false);
	}

	function setFields($login, $fields)
	{
		$qry = "";

		foreach ($fields as $key => $value) {
			$qry .= "$key = ?, ";
		}
		$qry = substr($qry, 0, -2);
		$q = $this->dbc->prepare("UPDATE ".TBL_USER." SET $qry WHERE login = ? AND active = '1'");
		$res = $q->execute(array_merge(array_values($fields), array($login)));

		if (!$res) {
			$this->html->errorMsg("Failed to set fields: ".$q->errorCode()." ".$q->errorInfo());
			return false;
		}

		return ($q->rowCount() == 1 ? true : false);
	}


/*
	function getFields($login, $fields, $type = "array")
	{
		$q = implode(", ", $fields);
		$res = mysql_query("SELECT $q FROM ".TBL_USER." WHERE login = '$login'");

		if (!$res) {
			die("Failed to get fields: ".mysql_error());
		}

		if ($type == "array") {
			return mysql_fetch_row($res);
		} else {
			return mysql_fetch_assoc($res);
		}
	}
 */	

	function checkPasswd($login, $passwd)
	{
		$passwd = sha1($passwd);

		$q = $this->dbc->prepare("SELECT COUNT(*) FROM ".TBL_USER." WHERE login = ? AND passwd = ? AND active = '1'");
		$res = $q->execute(array($login, $passwd));

		if (!$res) {
			$this->html->errorMsg("Failed to check password: ".$q->errorCode()." ".$q->errorInfo());
			$cnt = 0;
		} else {
			$cnt = $q->fetch();
			$cnt = $cnt[0];
		}

		return ($cnt == 1 ? true : false);
	}
}

?>
