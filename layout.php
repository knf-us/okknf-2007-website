<?php

$menu = array(
		"Główna" => "glowna",
		"Informacje" => "info",
		"Program" => "program",
		"Logowanie" => "reg",
		"Uczestnicy" => "uczestnicy",
		"Zakwaterowanie" => "zakwaterowanie",
		"Dojazd" => "dojazd",
		"Cieszyn" => "cieszyn",
		"Kontakt" => "kontakt",
	);

function theme_header($menu = true, $fullpage = false)
{
	$title = "Piknik Naukowy 2007";

	if ($fullpage == true) {
		$cntrstyle = "width: 100%;";
	}

	/* Stupid IE will be in quirks mode if we use the XML declaration.. */
	if (!broken_browser()) {
		echo '<?xml version="1.0" encoding="UTF-8"?>'."\n";
	}

	echo <<<HTML
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pl">
<head>
	<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="KNF UŚ US SPS Fizyka Koło Naukowe Fizyków" />
	<meta name="description" content="Piknik Naukowy 2007 -- VI Ogólnopolska Konferencja Kół Naukowych Fizyków" />
	<meta name="robots" content="index, follow" />
	<meta name="author" content="Michał Januszewski" />
	<title>$title</title>
	<link rel="stylesheet" href="css/main.css" type="text/css" />
</head>
<body>
<div id="container" style="$cntrstyle">
	<div id="header"></div>
HTML;
	theme_menu();

	return;
}

function theme_footer()
{
	global $lastmod;
	$mod = date("r", $lastmod);

	if ($_REQUEST["p"] != "reg") {
		$xhtml = '<a href="http://validator.w3.org/check?uri=referer"><img src="img/icons/xhtml11.png" alt="Valid XHTML 1.1" /></a><br />';
	}
		
	echo <<<HTML
	<div id="footer">
		<div id="footerleft">
			$xhtml
			<a href="http://jigsaw.w3.org/css-validator/check/referer"><img src="img/icons/css.gif" alt="Valid CSS" /></a>
		</div>
		<div id="footerright">
			(c) 2007, <a href="http://knf.us.edu.pl/">Koło Naukowe Fizyków UŚ</a>. Web design: <a href="http://people.gentoo.org/spock/">Michał Januszewski</a>.<br />
			Ostatnia modyfikacja: $mod<br />
		</div>
	</div>
	<div class="hshadow"></div>
</div>
</body>
</html>
HTML;

	return;
}

function theme_menu()
{
	global $menu;

	echo <<<HTML
	<div id="navbar">
HTML;

	$first = true;
	foreach ($menu as $desc => $uri) {
		if ($first) {
			echo "<ul>\n";
			$first = false;
		}

		if ($uri == "program") {
			$desc = "<b>$desc</b>";
		}

		if ($uri == "SEPARATOR") {
			echo "</ul>\n<br />\n";
			$first = true;
		} else if (substr($uri,0,7) == "http://") {
			echo "\t<li><a href=\"$uri\">$desc</a></li>\n";
		} else if ($uri == get_req_page()) {
			echo "\t<li class=\"selected\"><a href=\"$uri\">$desc</a></li>\n";
		} else {
			echo "\t<li><a href=\"$uri\">$desc</a></li>\n";
		}
	}

	echo '</ul></div>';
}

?>
