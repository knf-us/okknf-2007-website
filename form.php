<?php

class Form
{
	var $errors = array();
	var $known_fields = array();
	var $fields = array();
	var $err_fields = array();
	var $html;

	function Form(&$_html)
	{
		$this->html =& $_html;
	}

	function addError($err, $field="")
	{
		array_push($this->errors, $err);
		if ($field != "") {
			array_push($this->err_fields, $field);
		}
	}

	function setField($field, $val)
	{
		if (in_array($field, $this->known_fields)) {
			$this->fields["$field"] = $val;
		}
	}

	function setFields($fields, $strip = false)
	{
		foreach ($fields as $key => $val) {
			if (in_array($key, $this->known_fields)) {
				if ($strip) {
					$val = stripcslashes($val);
				}
				$this->fields["$key"] = $val;
			}
		}
	}

	function getFields()
	{
		return $this->fields;
	}

	function getField($name)
	{
		return $this->fields["$name"];
	}

	function printErrors()
	{
		if ($this->numErrors() > 0) {
			$s = implode("<br />", $this->errors);
			$this->html->errorMsg($s);
		}
	}

	function numErrors()
	{
		return count($this->errors);
	}

	function fval($field) {
		return $this->fields["$field"];
	}
}

class LoginForm extends Form
{
	var $user;

	function LoginForm(&$_html, &$_user)
	{
		$this->html =& $_html;
		$this->user =& $_user;
	}

	function printForm()
	{
		$this->printErrors();

echo <<<HTML
<div class="box formbox loginForm">
<h1>Logowanie do systemu</h1>

<form action="index.php" method="post">
<input type="hidden" name="cmd" value="login" />
<input type="hidden" name="p" value="reg" />
<ul class="formFields">
<li><label>Login</label><input type="text" name="login" /></li>
<li><label>Hasło</label><input type="password" name="passwd" /></li>
</ul>
<div class="buttons"><input type="submit" value="Zaloguj" /></div>
</form>
</div>
HTML;
	}

	function validate(&$input)
	{
		if ($input["login"] == "") {
			return false;
		} else if ($this->user->checkPasswd($input["login"], $input["passwd"])) {
			return true;
		} else {
			$this->addError("Nieprawidłowa nazwa użytkownika lub hasło.");
			return false;
		}
	}
}

class XMLForm extends Form
{
	var $xml, $xmlp, $dbc;
	var $currTag, $currAtt;
	var $field_desc;

	function XMLForm(&$_html, &$_dbc, $_xml)
	{
		$this->html =& $_html;
		$this->dbc  =& $_dbc;
		$this->xml  = $_xml;
		$xmlp = xml_parser_create("UTF-8");
		xml_set_object($xmlp, $this);
		xml_parser_set_option($xmlp,XML_OPTION_CASE_FOLDING,0);
		xml_set_element_handler($xmlp,'initTagStart','initTagEnd');
		xml_set_character_data_handler($xmlp, "initCharacterData");
		$this->parse($xmlp);
		xml_parser_free($xmlp);
	}

	function parse($p)
	{
		if (!($fp = fopen($this->xml, "r"))) {
			   die("could not open XML input");
		}

		while ($data = fread($fp, 4096)) {
			if (!xml_parse($p, $data, feof($fp))) {
			   die(sprintf("XML error: %s at line %d",
							xml_error_string(xml_get_error_code($p)),
							xml_get_current_line_number($p)));
			}
		}

		fclose($fp);
	}

	/* Functions used to parse the XML description of the form
	 * into the internal structures of this class. */
	function initCharacterData($parser, $text)
	{
		if ($this->currTag == "label") {
			$this->field_desc[$this->currAtt["for"]]["label"] .= $text;
		}
	}

	function initTagStart($parser, $name, $attribs)
	{
		$this->currTag = $name;
		$this->currAtt = $attribs;

		if ($name == "input" || $name == "select" || $name == "textarea" || $name == "checkbox") {
			array_push($this->known_fields, $attribs["name"]);
			if (!is_array($this->field_desc[$attribs["name"]])) {
				$this->field_desc[$attribs["name"]] = array();
			}
			$this->field_desc[$attribs["name"]]	= array_merge($this->field_desc[$attribs["name"]], $attribs);
		}

		/* Handle default values for checkboxes. */
		if ($attribs["type"] == "checkbox" && $attribs["default"] == "on" && $this->fields[$attribs["name"]] == "") {
			$this->fields[$attribs["name"]] = "on";
		}
	}

	function initTagEnd($parser, $name)
	{
		$this->currTag = "";
		return;
	}

	/* Functions used when the form is being displayed. */
	function showCharacterData($parser, $data) {
		print $data;
	}

	function showInputField($att, $value = true, $type = "")
	{
		echo '<input type="';
		if ($type != "") {
			echo $type;
		} else {
			echo $att["type"];
		}
		echo '" name="'.$att["name"].'"';
		if ($value) {
			echo ' value="'.$this->fields[$att["name"]].'"';
		}
		if ($att["required"] == "yes") {
			echo ' required="required"';
		}
		if ($att["pattern"] != "") {
			echo ' pattern="'.$att["pattern"].'"';
		}
		if (in_array($att["name"], $this->err_fields)) {
			echo ' class="error"';
		}
		echo ' />';
	}

	function showMiscSelect($att, $t)
	{
		echo '<select name="'.$att["name"].'">';
		foreach ($t as $id => $dsc) {
			echo '<option value="'.$id.'"';
			if ($this->fields[$att["name"]] == $id) {
				echo " selected";
			}
			echo ">$dsc</option>\n";
		}
		echo '</select>';
	}

	function showTextarea($att)
	{
		echo '<textarea name="'.$att["name"].'"';

		if ($att["cols"]) {
			echo ' cols="'.$att["cols"].'"';
		}

		if ($att["rows"]) {
			echo ' rows="'.$att["rows"].'"';
		}

		echo '>'.$this->fields[$att["name"]].'</textarea>';
	}

	function showCheckbox($att)
	{
		$v = $this->fields[$att["name"]];
		echo '<input type="checkbox" name="'.$att["name"].'"';
		
		if ($v == "on" || $v == "1" || ($att["default"] == "on" && $v != "off" && $v != 0)) {
			echo " checked=\"checked\"";
		}
		echo ' />';
	}
	
	function showUniSelect($att)
	{
		echo '<select name="'.$att["name"].'"';
		if (in_array($att["name"], $this->err_fields)) {
			echo ' class="error"';
		}
		echo '>';
		echo '<option value="0">inna placówka</option>';
		foreach ($this->dbc->query("SELECT id, name FROM unis ORDER BY name ASC") as $row) {
			echo '<option value="'.$row["id"].'"';
			if ($this->fields[$att["name"]] == $row["id"]) {
				echo " selected=\"selected\"";
			}
			echo ">$row[name]</option>\n";
		}
		echo '</select>';
	}

	function showDegreeSelect($att)
	{
		echo '<select name="'.$att["name"].'"';
		if (in_array($att["name"], $this->err_fields)) {
			echo ' class="error"';
		}
		echo '>';
		echo <<<HTML
<option value="none">(student)</option>
<option value="mgr">mgr</option>
<option value="inz">inż.</option>
<option value="mgr inz">mgr inż.</option>
<option value="dr">dr</option>
<option value="dr hab.">dr hab.</option>
<option value="doc">doc.</option>
<option value="prof">prof. dr hab.</option>
</select>
HTML;
	}

	function showTagStart($parser, $name, $att)
	{
		if ($name == "input") {
			switch ($att["type"]) {
			case "text":
			case "number":
			case "date":
			case "email":
			case "uri":
				$this->showInputField($att);
				break;

			case "uni":
				$this->showUniSelect($att);
				break;

			case "degree":
				$this->showDegreeSelect($att);
				break;

			case "room":
				$this->showMiscSelect($att,
					array("nopref" => "bez znaczenia", "double" => "dwuosobowy", "single" => "jednoosobowy"));
				break;

			case "food":
				$this->showMiscSelect($att,
					array("std" => "standardowe", "veg" => "wegetariańskie"));
				break;

			case "textarea":
				$this->showTextarea($att);
				break;

			case "checkbox":
				$this->showCheckbox($att);
				break;
				
			case "passwd":
			case "passwd_conf":
				$this->showInputField($att, $value=false, $type="password");
				break;
			}
		} else {
			echo "<$name";
			foreach ($att as $k => $v) {
				echo " $k=\"$v\"";
			}
			echo ">";
		}
	}

	function showTagEnd($parser, $name)
	{
		if ($name != "input") {
			print "</$name>";
		}
	}

	function fget($field, $att) {
		return $this->field_desc["$field"]["$att"];
	}

	function validate()
	{
		foreach ($this->known_fields as $f) {
			if ($this->fget($f,"ucfirst") == "yes") {
				$this->fields["$f"] = ucfirst($this->fval($f));
			}
			if ($this->fget($f,"required") == "yes" && $this->fval($f) == "") {
				$this->addError("Musisz wypełnić pole '".$this->fget($f,"label")."'.", $f);
			}
			if ($this->fget($f,"type") == "passwd" && $this->fval($f) != "") {
				if (strlen($this->fval($f)) < 5) {
					$this->addError("Podane hasło jest za krótkie.", $f);
				}
			}
			if ($this->fget($f,"type") == "passwd_conf") {
				if ($this->fval($f) != $this->fval($this->fget($f, "conffor"))) {
					$this->addError("Podane hasła muszą być jednakowe.", $f);
				}
			}
			if ($this->fget("$f","pattern") != "" && $this->fval($f) != "") {
				if (!ereg($this->fget($f,"pattern"), $this->fval($f))) {
					$this->addError("Nieprawidłowy format danych w polu '".$this->fget($f,"label")."'.", $f);
				}
			}
			if ($this->fget($f,"type") == "date") {
				if (strtotime($this->fval($f)) == -1) {
					$this->addError("Nieprawidłowa data w polu '".$this->fget($f,"label")."'.", $f);
				}
			}
			if ($this->fget($f,"type") == "number") {
				if (!ereg("^[0-9]*$", $this->fval($f))) {
					$this->addError("Pole '".$this->fget($f, "label")."' może zawierać tylko cyfry.", $f);
				}
			}
		}
	}

	/* Return list of fields in the database. */
	function dbFields($type="plain")
	{
		$r = array();

		foreach ($this->known_fields as $f) {
			if ($this->fget($f,"db") != "no") {
				if ($type == "assoc") {
					$r["$f"] = 1;
				} else {
					array_push($r, $f);
				}
			}
		}
		return $r;
	}

	/* Return a string with all field values. Useful for e-mails. */
	function textGetFields($fields = "")
	{
		$text = "";
		$tf = $this->known_fields;

		if (is_array($fields)) {
			$tf = $fields;
		}

		foreach ($tf as $f) {
			$text .= $this->fget($f, "label").": ".$this->fval($f)."\n";
		}

		return $text;
	}

	function diffFieldsWith(&$origdata)
	{
		return array_diff_assoc($this->fields, $origdata);
	}

	function diffDbFieldsWith(&$origdata)
	{
		$upd = $this->diffFieldsWith($origdata);
		return array_intersect_key($upd, $this->dbFields("assoc"));
	}

	function printForm()
	{
		$xmlp = xml_parser_create("UTF-8");
		xml_set_object($xmlp, $this);
		xml_parser_set_option($xmlp,XML_OPTION_CASE_FOLDING,0);
		xml_set_element_handler($xmlp,'showTagStart','showTagEnd');
		xml_set_character_data_handler($xmlp, "showCharacterData");
		$this->parse($xmlp);
		xml_parser_free($xmlp);
	}
}

class pdataForm extends XMLForm
{
	var $type = "register";
	var $login = "";

	function pdataForm(&$_html, &$_dbc, $type, $login = "") {
		$this->type = $type;
		if ($type == "register") {
			parent::XMLForm($_html, $_dbc, "pdata.xml");
		} else {
			parent::XMLForm($_html, $_dbc, "pdataedit.xml");
			$this->login = $login;
		}
	}

	function validate(&$input, $strip=false)
	{
		$this->setFields($input, $strip);

		foreach ($this->known_fields as $f) {
			if ($this->field_desc["$f"]["type"] != "passwd" && $this->field_desc["$f"]["type"] != "passwd_conf") {
				$this->fields["$f"] = trim($this->fields["$f"]);
			}
		
			if ($this->field_desc["$f"]["type"] == "checkbox") {
				if (!$this->fields["$f"]) {
					$this->fields["$f"] = "0";
				} else {
					$this->fields["$f"] = "1";
				}
			}
		}
		
		parent::validate();

		if ($this->type == "register") {
			if ($this->fval("login")) {
				$q = $this->dbc->prepare("SELECT COUNT(*) FROM ".TBL_USER." WHERE login = ?");
				$q->execute(array($this->fields["login"]));
				$r = $q->fetch();
				$q->closeCursor();
				if ($r[0] != 0) {
					$this->addError("Wybrany login jest już zajęty.", "login");
				}
			}
		}

		return $this->numErrors();
	}

	function printForm()
	{
echo <<<HTML
<div class="box formbox pdataForm">
<h1>Rejestracja</h1>
<div>
HTML;
		$this->printErrors();
		echo '</div><form action="index.php" method="post">';
		echo '<input type="hidden" name="cmd" value="'.$this->type.'" />';
		echo '<input type="hidden" name="p" value="reg" />';

		if ($this->type == "pdataedit") {
			echo '<input type="hidden" name="login" value="'.$this->login.'" />';
		}

		parent::printForm();
		echo <<<HTML
<div class="buttons"><input type="submit" name="subm" value="Wyślij" /></div>
</form>
</div>
HTML;
	}

}

?>
