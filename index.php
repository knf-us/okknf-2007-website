<?php
	require_once("common.php");
	theme_header();
?>
<div id="main">
	<div class="hshadow"></div>
	<div id="rightcontent">
		<h1>Ważne terminy:</h1>
		<ul>
			<li><b>16 kwiecień 2007</b><br /> <span style="color:red; font-weight:bold;">deadline rejestracji</span></li>
			<li><b>16 kwiecień 2007</b><br /> <span style="color:red; font-weight:bold;">deadline zgłaszania tytułów i abstraktów wykładów</span></li>
			<li><b>26 kwiecień 2007</b><br /> rozpoczęcie <span class="nobr">Pikniku Naukowego '2007</span></li>
			<li><b>29 kwiecień 2007</b><br /> zakończenie <span class="nobr">Pikniku Naukowego '2007</span></li>
		</ul>

		<h1>Patronat:</h1>
		<ul>
			<li><b>prof. dr hab. Janusz Janeczek</b><br />JM Rektor Uniwersytetu Śląskiego w Katowicach</li>
			<li><b>prof. dr hab. Reinhard Kulessa</b><br />Prezes Zarządu Głównego Polskiego Towarzystwa Fizycznego</li>
			<li><b>dr inż. Bogdan Ficek</b><br />Burmistrz miasta Cieszyna</li>
		</ul>

		<h1>Sponsorzy:</h1>
		<ul>
			<li><a href="http://www.us.edu.pl/">Uniwersytet Śląski</a></li>
			<li><a href="http://www.euroart.com.pl/">EuroArt</a></li>
		</ul>
	</div>
	<div id="maincontent">
<?php
	$page = get_req_page();

	if (file_exists("pages/$page.php")) {
		include("pages/$page.php");
		$lastmod = filemtime("pages/$page.php");
	} else {
		echo <<<HTML
<div class="box msgbox"><h1>Błąd</h1>
<p>Nie znaleziono strony.</p>
</div>
HTML;
		$lastmod = time();
	}
?>
	</div>
</div>
<?php
	theme_footer();
?>

